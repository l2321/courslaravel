<?php

use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\ArticlesJsonController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\Http\Controllers\DemoController;

Route::get('/', [DemoController::class, 'index'])->name('home');

Route::get('/users/{name}', [DemoController::class, 'name'])->whereAlphaNumeric('name')->name('name');

Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'submit'])->name('contact.submit');

Route::prefix('blog')->group(function() {
    Route::get('/', [ArticlesController::class, 'index'])->name('blog.articles.index');

    Route::get('/categories', [CategoriesController::class, 'index'])->name('blog.categories.index');
    Route::get('/categories/{category}', [CategoriesController::class, 'show'])->name('blog.categories.show');

    Route::get('auteurs', [UsersController::class, 'index'])->name('blog.users.index');
    Route::get('auteurs/{user}', [UsersController::class, 'show'])->name('blog.users.show');

    Route::get('crud/create', [ArticlesController::class, 'create'])->name('blog.crud.create');
    Route::post('crud/create', [ArticlesController::class, 'store'])->name('blog.crud.store');

    Route::get('crud/edit/{article}', [ArticlesController::class, 'edit'])->name('blog.crud.edit');
    Route::put('crud/edit/{article}', [ArticlesController::class, 'update'])->name('blog.crud.update');

    Route::delete('crud/delete/{article}', [ArticlesController::class, 'delete'])->name('blog.crud.destroy');

    Route::get('crud/categories/create', [CategoriesController::class, 'create'])->name('blog.crud.categories.create');
    Route::post('crud/categories/create', [CategoriesController::class, 'store'])->name('blog.crud.categories.store');

    Route::get('crud/categories/edit/{category}', [CategoriesController::class, 'edit'])->name('blog.crud.categories.edit');
    Route::put('crud/categories/edit/{category}', [CategoriesController::class, 'update'])->name('blog.crud.categories.update');

    Route::delete('crud/categories/delete/{category}', [CategoriesController::class, 'destroy'])->name('blog.crud.categories.destroy');

    Route::get('/{article}', [ArticlesController::class, 'show'])->name('blog.articles.show');
});

/**
 * Articles JSON
 */
Route::get('articles-json', [ArticlesJsonController::class, 'index'])->name('articles.json.index');
Route::get('articles-json/{iteration}', [ArticlesJsonController::class, 'show'])->whereNumber('iteration')->name('articles.json.show');
Route::get('authors-json/{name}', [ArticlesJsonController::class, 'author'])->name('articles.json.author');
