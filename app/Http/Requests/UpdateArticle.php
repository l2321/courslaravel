<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'         => ['required', 'min:3', 'max:255', Rule::unique('articles', 'title')->ignore($this->route('article'))],
            'content'       => ['required', 'min:3'],
            'categories'    => ['min:1', Rule::exists('categories', 'id')],
            'author_id'     => ['min:1', Rule::exists('users', 'id')],
        ];
    }
}
