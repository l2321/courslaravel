<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'        =>  $this->id,
            'title'     =>  strtoupper($this->title),
            'image_url' =>  $this->image_url,
            'author'    =>  new AuthorResource($this->author),
            'categories'=>  CategoryResource::collection($this->categories),
            'published_at'  =>  $this->published_at->format('d M Y H:i'),
        ];
    }
}
