<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateArticle;
use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ArticlesController extends Controller
{
    /**
     * Liste des articles
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $articles = Article::orderBy('published_at', 'DESC')->paginate(6);
        return view('blog.index', compact('articles'));
    }

    /**
     * Article directement chargé grace à la règle dans RouteServiceProvider
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Article $article)
    {
        return view('blog.article', compact('article'));
    }

    /**
     * Formulaire de création d'un article
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $article = new Article();
        $users = User::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        return view(' blog.crud.create', compact('article', 'categories', 'users'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //  On valide les champs récupérés depuis le formulaire
        $inputs = $this->validate($request, [
            'title'         => ['required', 'min:3', 'max:255', Rule::unique('articles', 'title')],
            'content'       => ['required', 'min:3'],
            'categories'    => ['min:1', Rule::exists('categories', 'id')],
            'author_id'     => ['min:1', Rule::exists('users', 'id')],
        ]);

        //  Si un fichier est renseigné, on le sauvegarde et on génère son URL pour le passer dans notre
        //  tableau d'inputs
        if( $request->hasFile('image') ) {
            $inputs['image_url'] = asset(public_path('storage/' . $request->file('image')->store('images')));
        }

        //  On rajoute manuellement la date de publication
        $inputs['published_at'] = now();

        //  On crée notre article
        $article = Article::create($inputs);

        //  On ajoute les catégories depuis le select multiple
        $article->categories()->attach($request->input('categories'));

        //  On redirige sur la liste des articles
        return redirect()->route('blog.articles.index')->with('success', 'Article créé avec succès');
    }

    /**
     * @param \App\Models\Article $article
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Article $article)
    {
        $users = User::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        return view('blog.crud.edit', compact('article', 'users', 'categories'));
    }

    /**
     * @param \App\Http\Requests\UpdateArticle $request
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Article $article, UpdateArticle $request)
    {
        $inputs = $request->all();

        if( $request->hasFile('image') ) {
            $inputs['image_url'] = asset(public_path('storage/' . $request->file('image')->store('images')));
            /*
             * Impossible dans notre configuration car on stocke l'url entière en Base de Donnée, ce qui
             * est une mauvaise pratique
            if( file_exists(storage_path('app/public/images/' . $article->image_url)) ) {
                unlink(storage_path('app/public/images/' . $article->image_url));
            }
            */
        }

        $article->update($inputs);

        /**
         * Synchronise les IDs des catégories avec l'article mis à jour. La méthode sync permet de
         * supprimer les anciennes relations et d'en créer de nouvelles.
         */
        $article->categories()->sync($request->input('categories'));

        //  On redirige sur la liste des articles
        return redirect()->route('blog.articles.index')->with('success', 'Article mis à jour');
    }

    /**
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Article $article)
    {
        $article->delete();
        return redirect()->route('blog.articles.index')->with('success', 'Article supprimé');
    }
}
