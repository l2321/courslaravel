<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticlesController extends Controller
{
    public function index()
    {
        return ArticleResource::collection(Article::with('author', 'categories')->paginate(10));
    }

    public function show($id)
    {
        $article = Article::with('author', 'categories')->findOrFail($id);
        return new ArticleResource($article);
    }
}
