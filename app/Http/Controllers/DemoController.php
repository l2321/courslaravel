<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class DemoController extends Controller
{
    public function index(): View
    {
        $students = [
            'Hussein',
            'Romain',
            'Jayson',
            'Lucas',
            'Sarah',
        ];

        return view('home', compact('students'));
    }

    public function name($name)
    {
        /*
        return view('name', [
            'name'  =>  $name,
        ]);

        return view('name')->with([
            'name'  =>  $name,
        ]);
        */

        return view('name', compact('name'));
    }
}
