<?php
namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoriesController extends Controller
{
    /**
     * Liste des catégories
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $categories = Category::withCount('articles')->orderBy('name', 'ASC')->get();
        return view('blog.categories', compact('categories'));
    }

    /**
     * Vue d'une catégorie
     *
     * @param \App\Models\Category $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Category $category)
    {
        $articles = Article::whereHas('categories', function($query) use ($category) {
            return $query->where('category_id', $category->id);
        })->paginate(6);

        return view('blog.category', compact('category', 'articles'));
    }

    /**
     * Formulaire de création d'une catégorie
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $category = new Category();
        return view('blog.crud.categories.create', compact('category'));
    }

    /**
     * Sauvegarde (création) d'une catégorie
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      =>  ['required', 'min:3', 'max:255', Rule::unique('categories', 'name')],
        ]);
        Category::create($request->all());
        return redirect()->route('blog.categories.index');
    }

    /**
     * Formulaire d'édition d'une catégorie
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Category $category)
    {
        return view('blog.crud.categories.edit', compact('category'));
    }

    /**
     * Sauvegarde (mise à jour) d'une catégorie
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name'      =>  ['required', 'min:3', 'max:255', Rule::unique('categories', 'name')->ignore($category->id)],
        ]);

        $category->update([
            'name'  =>  $request->input('name'),
        ]);
        return redirect()->route('blog.categories.index');
    }

    /**
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('blog.categories.index');
    }
}
