<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::withCount('articles')->orderBy('created_at', 'DESC')->get();
        return view('blog.users', compact('users'));
    }

    public function show(User $user)
    {
        $articles = Article::where('author_id', $user->id)->paginate(9);
        return view('blog.user', compact('user', 'articles'));
    }
}
