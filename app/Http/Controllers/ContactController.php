<?php
namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'name'      =>  ['required'],
            'email'     =>  ['required', 'email'],
            'firstname'  =>  ['required'],
            'message'   =>  ['required', 'min:10']
        ]);

        Mail::to('alexandre@laboiteacode.fr')->send(new ContactMail($request->all()));
        return redirect()->route('contact.index')->with('success', 'Votre message a bien été envoyé');
    }
}
