<div class="col">
    <div class="card shadow-sm">
        <img class="card-img-top" height="250" src="{{ $article->urlToImage }}" />

        <div class="card-body">
            <h3><a title="{{ $article->title }}" href="{{ route('articles.show', $loop->iteration) }}">{{ $article->title }}</a></h3>
            <p class="card-text">{!! $article->description !!}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div>{{ \Carbon\Carbon::parse($article->publishedAt)->format('d/m/Y H:i') }}</div>
                @if( $article->author !== null )
                <div><a href="{{ route('articles.author',$article->author) }}">{{ $article->author }}</a></div>
                @endif
            </div>
        </div>
    </div>
</div>
