@extends('layouts.app')

@section('content')

    <div class="text-center d-flex flex-column justify-content-center align-items-center" style="background: url('{{ $article->urlToImage }}') no-repeat center center; background-size: cover; width: 100%; height: 500px">
        <div class="container">
            <h1 class="display-5 fw-bold">{{ $article->title }}</h1>
            <p class="lead">Le {{ \Carbon\Carbon::parse($article->publishedAt)->format('d/m/Y H:i')  }} par {{ $article->author }}</p>
        </div>
    </div>

    <div class="container py-5">

        <br /><br />
        {!! $article->description !!}

        <div class="d-block py-5">
            <a href="{{ route('articles.index') }}" class="btn btn-sm btn-primary">Retourner à la liste des articles</a>
        </div>
    </div>
@endsection
