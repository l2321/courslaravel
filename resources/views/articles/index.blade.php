@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="py-5">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                @foreach( $articles as $article )
                    @include('articles.article', ['article' => $article])
                @endforeach
            </div>
        </div>
    </div>

@endsection
