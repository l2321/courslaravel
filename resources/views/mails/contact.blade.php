@component('mail::message')
Un utilisateur vous a contacté via le formulaire de contact de votre site web.

{{ $name }} {{ $firstname }} ({{ $email }}) vous a envoyé le message suivant :

@component('mail::panel')
{{ $content }}
@endcomponent

@endcomponent
