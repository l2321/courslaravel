<div class="container">
    <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
        </a>

        <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
            <li><a href="{{ route('home') }}" class="nav-link px-2 link-secondary">Accueil</a></li>
            <li><a href="{{ route('blog.articles.index') }}" class="nav-link px-2 link-dark">Articles</a></li>
            <li><a href="{{ route('blog.categories.index') }}" class="nav-link px-2 link-dark">Catégories</a></li>
            <li><a href="{{ route('blog.users.index') }}" class="nav-link px-2 link-dark">Auteurs</a></li>
        </ul>

        <div class="col-md-4 text-end">
            <a href="{{ route('blog.crud.create') }}" class="btn btn-outline-primary me-2">Créer un article</a>
            <a href="{{ route('blog.crud.categories.create') }}" class="btn btn-outline-success me-2">Créer une catégorie</a>
        </div>
    </header>

    @if( session()->has('success') )
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
</div>
