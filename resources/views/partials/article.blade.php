<div class="col">
    <div class="card shadow-sm">
        <img class="card-img-top" height="250" src="{{ $article->urlToImage }}" />

        <div class="card-body">
            <h3><a title="{{ $article->title }}" href="{{ route('blog.articles.show', $article->id) }}">{{ $article->title }}</a></h3>
            <p class="card-text">{!! $article->content !!}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div>{{ ($article->publishedDate) }}</div>
                @if( $article->author !== null )
                    <div><a href="{{ route('blog.users.show', $article->author->id) }}">{{ $article->author->name }}</a></div>
                @endif
            </div>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><strong>Catégories</strong></li>
            @foreach( $article->categories as $cat )
                <li class="list-group-item"><a href="{{ route('blog.categories.show', $cat->id) }}">{{ $cat->name }}</a></li>
            @endforeach
        </ul>
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <a href="{{ route('blog.crud.edit', $article->id) }}" class="btn btn-sm btn-primary">Modifier</a>
                <form action="{{ route('blog.crud.destroy', $article->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">Supprimer</button>
                </form>
            </div>
        </div>
    </div>
</div>
