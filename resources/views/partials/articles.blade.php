<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    @foreach( $articles as $article )
        @include('partials.article')
    @endforeach
</div>

<div class="py-4">
{!! $articles->links() !!}
</div>
