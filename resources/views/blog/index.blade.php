@extends('layouts.app')

@section('content')
    <div class="container py-5">
        <h1>Derniers articles publiés</h1>

        @include('partials.articles', ['articles' => $articles])
    </div>
@endsection
