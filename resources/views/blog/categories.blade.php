@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Liste des catégories</h1>

        <a href="{{ route('blog.crud.categories.create') }}" class="btn btn-success btn-sm my-3">Créer une catégorie</a>

        <ul class="w-100">
            @foreach( $categories as $category )
                <li class="d-flex ">
                    <a href="{{ route('blog.categories.show', $category->id) }}">{{ $category->name }}</a> ({{ $category->articles_count }} articles)
                    <a class="btn btn-sm btn-primary ms-3" href="{{ route('blog.crud.categories.edit', [$category->id]) }}">Modifier</a></li>
                    <form method="POST" action="{{ route('blog.crud.categories.destroy', $category->id) }}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger ms-3 btn-sm">Supprimer</button>
                    </form>
            @endforeach
        </ul>
    </div>

@endsection
