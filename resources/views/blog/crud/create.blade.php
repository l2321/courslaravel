@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Créer un nouvel article</h1>

        @include('blog.crud.form', [
            'action' => route('blog.crud.store'),
            'method' => 'POST',
        ])

    </div>


@endsection
