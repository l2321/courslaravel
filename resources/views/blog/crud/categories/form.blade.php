@if( $errors->any() )
    <div class="alert alert-danger">
        {{ $errors->first() }}
    </div>
@endif

<form action="{{ $action }}" method="POST">
    @csrf
    @method($method)

    <label for="name">Nom de la catégorie</label>
    <input type="text" name="name" class="form-control" value="{{ old('name', $category->name) }}">

    <button type="submit" class="btn mt-4 btn-primary">Enregistrer</button>
</form>
