@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Créer une nouvelle catégorie</h1>

        @include('blog.crud.categories.form', [
            'action' => route('blog.crud.categories.store'),
            'method' => 'POST',
        ])

    </div>


@endsection
