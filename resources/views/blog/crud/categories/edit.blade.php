@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Modifier une catégorie catégorie</h1>

        @include('blog.crud.categories.form', [
            'action' => route('blog.crud.categories.update', $category->id),
            'method' => 'PUT',
        ])

    </div>


@endsection
