@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Modifier un article</h1>

        @include('blog.crud.form', [
            'action' => route('blog.crud.update', $article->id),
            'method' => 'PUT',
        ])

    </div>


@endsection
