@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Liste des utilisateurs</h1>

        <ul>
            @foreach( $users as $user )
                <li><a href="{{ route('blog.users.show', $user->id) }}">{{ $user->name }}</a> ({{ $user->articles_count }} articles)</li>
            @endforeach
        </ul>
    </div>

@endsection
