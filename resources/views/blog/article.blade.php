@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>{{ $article->title }}</h1>
        <img src="{{ $article->image_url }}" alt="{{ $article->title }}" class="w-100 img-fluid d-block">
        {!! $article->content !!}

        <div class="d-flex pt-4 justify-content-between align-items-center">
            @if( $article->author !== null )
                <div><a href="{{ route('blog.users.show', $article->author->id) }}">{{ $article->author->name }}</a></div>
            @endif
            <div>
                @foreach( $article->categories as $category )
                    <a href="{{ route('blog.categories.show', $category->id) }}">{{ $category->name }}</a>
                @endforeach
            </div>
        </div>
    </div>


@endsection
