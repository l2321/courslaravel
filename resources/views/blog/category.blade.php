@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Articles de la catégorie "{{ $category->name }}"</h1>

        @include('partials.articles', ['articles' => $articles])
    </div>

@endsection
