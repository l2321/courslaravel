@extends('layouts.app')

@section('content')

    <div class="container py-5">
        <h1>Articles de l'utilisateur "{{ $user->name }}"</h1>

        @include('partials.articles', ['articles' => $articles])
    </div>

@endsection
