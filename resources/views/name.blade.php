@extends('layouts.app')

@section('content')
    <h3>Salut {{ $name }}</h3>

    @if( $name === "Jean" )

    <p>Toi je t'aime bien !</p>

    @elseif( $name === 'Patrick' )

    <p>Heu, on se connait ?</p>

    @else

    <p>Bienvenue jeune Padawan !</p>

    @endif

@endsection
