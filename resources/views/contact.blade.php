@extends('layouts.app')

@section('content')
    <div class="container py-5">
        <h1>Nous contacter</h1>

        @if( $errors->any() )
            <div class="alert alert-danger">
                <ul>
                    @foreach( $errors->all() as $error )
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form class="form" method="POST" action="{{ route('contact.submit') }}">
            @csrf

            <div class="form-group mb-3">
                <label for="name">Nom</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Votre nom" value="{{ old('name') }}">
            </div>

            <div class="form-group mb-3">
                <label for="email">Prénom</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Votre prénom" value="{{ old('firstname') }}">
            </div>

            <div class="form-group mb-3">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Votre email" value="{{ old('email') }}">
            </div>

            <div class="form-group mb-3">
                <label for="message">Message</label>
                <textarea class="form-control" id="message" name="message" rows="3">{{ old('message') }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Envoyer</button>

        </form>
    </div>
@endsection
